package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

func (r13 rot13Reader) Read(b []byte) (int, error) {
	// temp_b := []byte{} 这里新造一个字节切片接受，会导致程序卡住
	r, err := r13.r.Read(b)
	for i, v := range b {
		switch {
		case 65 <= v && v <= 90:
			new_v := v + 13
			if new_v <= 90 {
				b[i] = new_v
			} else {
				new_v = new_v - 26
				b[i] = new_v
			}
		case 97 <= v && v <= 122:
			new_v := v + 13
			if new_v <= 122 {
				b[i] = new_v
			} else {
				new_v = new_v - 26
				b[i] = new_v
			}
		}
	}

	return r, err

}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
